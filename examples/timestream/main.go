package main

import (
	"context"
	"fmt"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/timestreamwrite"
)

func main() {
	lambda.Start(HandleRequest)
}

func HandleRequest(ctx context.Context) (interface{}, error) {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	writeSvc := timestreamwrite.New(sess)
	listDatabasesMaxResult := int64(15)
	listDatabasesInput := &timestreamwrite.ListDatabasesInput{
		MaxResults: &listDatabasesMaxResult,
	}

	listDatabasesOutput, err := writeSvc.ListDatabases(listDatabasesInput)
	if err != nil {
		return nil, fmt.Errorf("err listDatabasesOutput: %s", err)
	}

	fmt.Printf("list of databases: %+v\n", listDatabasesOutput)

	return "all works well!", nil
}
