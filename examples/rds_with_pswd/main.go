package main

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"net/url"
	"os"

	"github.com/aws/aws-lambda-go/lambda"
	_ "github.com/go-sql-driver/mysql"
)

func main() {
	lambda.Start(HandleRequest)
}

func HandleRequest(ctx context.Context) (interface{}, error) {
	dbHost, ok := os.LookupEnv("DB_HOST")
	if !ok {
		return nil, errors.New("DB_HOST not found")
	}

	dbName, ok := os.LookupEnv("DB_NAME")
	if !ok {
		return nil, errors.New("DB_NAME not found")
	}

	dbPort, ok := os.LookupEnv("DB_PORT")
	if !ok {
		return nil, errors.New("DB_PORT not found")
	}

	dbPswd, ok := os.LookupEnv("DB_PSWD")
	if !ok {
		return nil, errors.New("DB_PSWD not found")
	}

	dbUser, ok := os.LookupEnv("DB_USER")
	if !ok {
		return nil, errors.New("DB_USER not found")
	}

	tz := url.QueryEscape("America/Los_Angeles") // Compatibility to ensure timestamps are read as Legacy would
	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=True&loc=%s", dbUser, dbPswd, dbHost, dbPort, dbName, tz))
	if err != nil {
		return nil, fmt.Errorf("err opening db: %s", err)
	}

	fmt.Printf("got db:> %+v\n", db)

	fmt.Println("performing db.ping...")
	if err = db.Ping(); err != nil {
		return nil, fmt.Errorf("err pinging db: %s", err)
	}

	return "all works well!", nil
}
