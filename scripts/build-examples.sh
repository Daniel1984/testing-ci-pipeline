#!/bin/sh
set -e

rootDir=$PWD

for d in ../examples/*; do
  echo "Building $d"
  cd $d
  CGO_ENABLED=0 GOOS=linux go build main.go
  zip main.zip main
  cd $rootDir
done
