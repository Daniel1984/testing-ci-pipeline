variable "table_name" {
  description = "name of timestream table"
  type        = string
}

variable "database_name" {
  description = "name of timestream database"
  type        = string
}

variable "env" {
  description = "name of the environment"
  type        = string
}

variable "magnetic_store_retention_period_in_days" {
  description = "The duration for which data must be stored in the magnetic store. Minimum value of 1. Maximum value of 73000"
  type        = number
  default     = 30
}

variable "memory_store_retention_period_in_hours" {
  description = "The duration for which data must be stored in the memory store. Minimum value of 1. Maximum value of 8766."
  type        = number
  default     = 8
}
