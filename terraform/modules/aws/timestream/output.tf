output "database_name" {
  value = aws_timestreamwrite_database.cluster.id
}

output "database_arn" {
  value = aws_timestreamwrite_database.cluster.arn
}

output "database_tags" {
  value = aws_timestreamwrite_database.cluster.tags_all
}

output "database_kms_key_id" {
  value = aws_timestreamwrite_database.cluster.kms_key_id
}

output "database_table_count" {
  value = aws_timestreamwrite_database.cluster.table_count
}

output "table_name" {
  value = aws_timestreamwrite_table.table.id
}

output "table_arn" {
  value = aws_timestreamwrite_table.table.arn
}

output "table_tags" {
  value = aws_timestreamwrite_table.table.tags_all
}
