resource "aws_timestreamwrite_database" "cluster" {
  database_name = var.database_name

  tags = {
    Environment = var.env
  }
}

resource "aws_timestreamwrite_table" "table" {
  database_name = aws_timestreamwrite_database.cluster.database_name
  table_name    = var.table_name

  retention_properties {
    magnetic_store_retention_period_in_days = var.magnetic_store_retention_period_in_days
    memory_store_retention_period_in_hours  = var.memory_store_retention_period_in_hours
  }

  tags = {
    Environment = var.env
  }
}
