output "id" {
  value = aws_rds_cluster.database.id
}

output "arn" {
  value = aws_rds_cluster.database.arn
}

output "cluster_identifier" {
  value = aws_rds_cluster.database.cluster_identifier
}

output "database_name" {
  value = aws_rds_cluster.database.database_name
}

output "username" {
  value = aws_rds_cluster.database.master_username
}

output "password" {
  value     = aws_rds_cluster.database.master_password
  sensitive = true
}

output "port" {
  value = aws_rds_cluster.database.port
}

output "endpoint" {
  value = aws_rds_cluster.database.endpoint
}

output "read_endpoint" {
  value = aws_rds_cluster.database.reader_endpoint
}
