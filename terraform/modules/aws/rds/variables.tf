variable "vpc_security_group_ids" {
  description = "list of security group ids"
  type        = list(string)
}

variable "subnet_ids" {
  description = "list of subnet ids"
  type        = list(string)
}

variable "number_of_instances" {
  description = "number of discrete instances within the cluster"
  type        = number
  default     = 1
}

variable "instance_class" {
  description = "AWS RDS instance size for cluster - https://aws.amazon.com/rds/instance-types/"
  type        = string
  default     = "db.r4.large"
}

variable "cluster_identifier" {
  description = "name to apply to cluster"
  type        = string
}

variable "publicly_accessible" {
  description = "bool to control if instance is publicly accessibl"
  type        = bool
  default     = false
}

variable "availability_zones" {
  description = "the availability zones of the instance"
  type        = list(string)
}

variable "name" {
  description = "name of db"
  type        = string
}

variable "env" {
  description = "name of the environment"
  type        = string
}

variable "master_username" {
  description = "the master username for the database"
  type        = string
}
