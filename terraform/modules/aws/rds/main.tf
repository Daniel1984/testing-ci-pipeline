resource "aws_rds_cluster_instance" "cluster_instances" {
  count               = var.number_of_instances
  identifier          = "${var.env}-aurora-cluster-instance-${count.index}"
  cluster_identifier  = aws_rds_cluster.database.id
  instance_class      = var.instance_class
  engine              = aws_rds_cluster.database.engine
  engine_version      = aws_rds_cluster.database.engine_version
  publicly_accessible = var.publicly_accessible
}

resource "aws_rds_cluster" "database" {
  cluster_identifier     = var.cluster_identifier
  db_subnet_group_name   = aws_db_subnet_group.db_subnet_group.name
  availability_zones     = var.availability_zones
  vpc_security_group_ids = var.vpc_security_group_ids
  database_name          = var.name
  master_username        = var.master_username
  master_password        = random_password.rng.result
  skip_final_snapshot    = true

  lifecycle {
    ignore_changes = [
      # engine_version, ?
      master_username,
      master_password,
    ]
  }
}

resource "aws_db_subnet_group" "db_subnet_group" {
  name       = "${var.cluster_identifier}-subnet-group"
  subnet_ids = var.subnet_ids
}

resource "random_password" "rng" {
  length  = 16
  special = false

  keepers = {
    cluster_identifier = "${var.cluster_identifier}"
  }
}

