#-----------------------------------------------------------------------------------
# create execution role and policy for apigw
#-----------------------------------------------------------------------------------
data "template_file" "role_policy" {
  template = file("${path.module}/templates/role_policy.json")
}

data "template_file" "iam_policy" {
  template = file("${path.module}/templates/iam_policy.json")
  vars = {
    create_email_lambda_arn = var.create_email_lambda_arn
  }
}

resource "aws_iam_role" "lambda_role" {
  name               = "${var.env}-${var.region}-directory-listing-lambda-role"
  assume_role_policy = data.template_file.role_policy.rendered
}

resource "aws_iam_role_policy_attachment" "basic_execution_policy" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "vpc_attachment_policy" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

resource "aws_iam_policy" "lambda_policy" {
  name   = "${var.env}-${var.region}-directory-listing-lambda-policy"
  policy = data.template_file.iam_policy.rendered
}

resource "aws_iam_role_policy_attachment" "lambda_policy" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.lambda_policy.arn
}
