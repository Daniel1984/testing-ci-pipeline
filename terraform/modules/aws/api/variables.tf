variable "env" {
  type    = string
  default = "dev"
}

variable "region" {
  type = string
}

variable "description" {
  type = string
}

variable "name" {
  type = string
}

variable "tags" {
  default = {}
  type    = map(string)
}

# variable "read_api_handler_arn" {
#   type = string
# }

variable "create_email_lambda_arn" {
  type = string
}

variable "api_deployed_at" {
  description = "api gateway deployed at ts. Used to force api gateway deployment in aws"
}

variable "endpoint_configuration" {
  default = ["PRIVATE"]
  type    = list(string)
}

variable "stage_name" {
  default = "v1"
  type    = string
}

variable "title" {
  type = string
}

variable "xray_tracing_enabled" {
  default = true
  type    = bool
}
