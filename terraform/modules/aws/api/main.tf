#-----------------------------------------------------------------------------------
# create REST API gateway
#-----------------------------------------------------------------------------------
data "template_file" "apigw_policy" {
  template = file("${path.module}/templates/apigw_policy.json")
}

resource "aws_api_gateway_rest_api" "api" {
  name        = var.name
  description = var.description
  body        = data.template_file.api.rendered
  policy      = data.template_file.apigw_policy.rendered

  endpoint_configuration {
    types = var.endpoint_configuration
  }

  tags = {
    env = var.env
  }
}

resource "aws_api_gateway_deployment" "api" {
  rest_api_id = aws_api_gateway_rest_api.api.id

  variables = {
    api_version = md5(file("${path.module}/openapi/openapi.v1.yml"))
    deployed_at = var.api_deployed_at
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "api" {
  deployment_id = aws_api_gateway_deployment.api.id
  rest_api_id   = aws_api_gateway_rest_api.api.id
  stage_name    = var.stage_name

  tags = {
    env = var.env
  }

  xray_tracing_enabled = var.xray_tracing_enabled
}

resource "aws_api_gateway_method_settings" "api" {
  method_path = "*/*"
  rest_api_id = aws_api_gateway_rest_api.api.id
  stage_name  = aws_api_gateway_stage.api.stage_name

  settings {
    metrics_enabled = true
  }
}

data "template_file" "api" {
  template = file("${path.module}/openapi/openapi.v1.yml")

  vars = {
    title                   = var.title
    description             = var.description
    region                  = var.region
    create_email_lambda_arn = var.create_email_lambda_arn
    # read_api_handler_arn  = var.read_api_handler_arn
    role_arn = aws_iam_role.lambda_role.arn
  }
}
