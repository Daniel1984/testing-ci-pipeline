output "invoke_url" {
  value = aws_api_gateway_stage.api.invoke_url
}

output "stage" {
  value = aws_api_gateway_stage.api.stage_name
}

output "host" {
  value = element(split("/", aws_api_gateway_deployment.api.invoke_url), 2)
}

output "execution_arn" {
  value = aws_api_gateway_rest_api.api.execution_arn
}

output "id" {
  value = aws_api_gateway_rest_api.api.id
}
