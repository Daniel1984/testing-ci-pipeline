#-------------------------------------------------------------------------------------------
# define TF and dependency versions
#-------------------------------------------------------------------------------------------
terraform {
  required_version = "0.15.4"

  required_providers {
    aws = {
      version = ">= 3.42.0"
      source  = "hashicorp/aws"
    }
  }
}
