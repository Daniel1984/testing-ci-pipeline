variable "region" {
  default = "eu-central-1"
  # default = "us-east-1"
}

# variable "profile" {}

variable "availability_zones" {
  default     = ["eu-central-1a", "eu-central-1b", "eu-central-1c"]
  description = "list of availability zone ids"
  type        = list(string)
}

variable "subnet_ids" {
  default     = ["subnet-46d4820b", "subnet-a2eafec9", "subnet-68eead15"]
  description = "list of subnet ids"
  type        = list(string)
}

variable "security_group_ids" {
  default     = ["sg-059c8969"]
  description = "list of security group ids"
  type        = list(string)
}

variable "aurora_db_username" {
  default     = "admin"
  description = "aurora db username"
  type        = string
}

variable "aurora_table_name" {
  default     = "ContentModeration"
  description = "aurora db name"
  type        = string
}

variable "timestream_table_name" {
  default     = "ContentModeration"
  description = "(Required) The name of the Timestream table"
  type        = string
}

variable "domain" {
  default     = "content-moderation"
  description = "domain"
  type        = string
}

variable "consul_token" {
  default     = null
  description = "consul token"
  type        = string
}

variable "consul_datacenter_list" {
  type        = list(string)
  description = "List of consul datacenters to register service within. Default: [\"use1\", \"euw1\"]"
  default     = []
  # default     = ["use1", "euw1"]
}

variable "consul_address" {
  type    = string
  default = "consul.aws.sussexdirectories.com:8500"
}

# variable "api_deployed_at" {
#   description = "api gateway deployed at ts. Used to force api gateway deployment in aws"
# }
