locals {
  env                 = terraform.workspace == "default" ? "prod" : terraform.workspace
  aurora_db_instances = local.env == "prod" ? "3" : "1"
  aurora_db_size      = local.env == "prod" ? "db.r5.xlarge" : "db.t3.medium"
}
