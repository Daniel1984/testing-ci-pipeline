provider "aws" {
  region = var.region
  # profile is for ease of testing locally
  # profile = var.profile

  # below left to test in CI
  # assume_role {
  #   role_arn = "arn:aws:iam::055603546736:role/terraform"
  #   session_name = "terraform"
  # }

  # skip_credentials_validation = true
}

#-----------------------------------------------------------------------------------
# define terraform backend
#-----------------------------------------------------------------------------------
terraform {
  # backend "consul" {}
  backend "s3" {
    bucket  = "dan-infrastructure-tfstate"
    key     = "terraform.tfstate"
    region  = "eu-central-1"
  }
}

# provider "consul" {
#   address = var.consul_address
#   token   = var.consul_token
# }


#-----------------------------------------------------------------------------------
# create aurora db instance
#-----------------------------------------------------------------------------------
# module "aurora" {
#   source                 = "./modules/aws/rds"
#   env                    = local.env
#   name                   = var.aurora_table_name
#   cluster_identifier     = "${local.env}-${var.domain}-aurora-db-cluster"
#   number_of_instances    = local.aurora_db_instances
#   instance_class         = local.aurora_db_size
#   availability_zones     = var.availability_zones
#   subnet_ids             = var.subnet_ids
#   vpc_security_group_ids = var.security_group_ids
#   master_username        = var.aurora_db_username
# }

# resource "vault_generic_secret" "aurora" {
#   path = "secret/${local.env}-${var.domain}/aurora"
#   data_json = jsonencode({
#     DB_USER : module.aurora.username,
#     DB_PSWD : module.aurora.password
#   })
# }

#-----------------------------------------------------------------------------------
# create timestream db instance
#-----------------------------------------------------------------------------------
# module "timestream" {
#   source        = "./modules/aws/timestream"
#   database_name = "${local.env}-${var.domain}-db-cluster"
#   table_name    = var.timestream_table_name
#   env           = local.env
# }

#-----------------------------------------------------------------------------------
# role for lambda to access specified resources
#-----------------------------------------------------------------------------------
data "template_file" "lambda_policy" {
  template = file("templates/lambda_policy.json")
}

data "template_file" "lambda_role" {
  template = file("templates/lambda_role.json")
}

resource "aws_iam_policy" "content_moderation" {
  name        = "${local.env}-content-moderation-policy"
  description = "policy to allow lambda use specified resources"
  policy      = data.template_file.lambda_policy.rendered
}

resource "aws_iam_role" "content_moderation" {
  name               = "${local.env}-content-moderation-role"
  assume_role_policy = data.template_file.lambda_role.rendered
}

resource "aws_iam_role_policy_attachment" "content_moderation" {
  role       = aws_iam_role.content_moderation.name
  policy_arn = aws_iam_policy.content_moderation.arn
}

#-----------------------------------------------------------------------------------
# email content ingest lambda / initial step of ingest pipeline
#-----------------------------------------------------------------------------------
module "create_email_lambda" {
  source        = "./modules/aws/lambda"
  function_name = "${local.env}-create-email"
  description   = "lambda to validate request payload and persist email"
  role_arn      = aws_iam_role.content_moderation.arn

  vpc_config = {
    subnet_ids         = var.subnet_ids
    security_group_ids = var.security_group_ids
  }

  environment = {
    REGION = var.region
  }

  tags = {
    Environment = local.env
  }
}

# #-----------------------------------------------------------------------------------
# # lambda to keep track of email state change in timestream
# #-----------------------------------------------------------------------------------
# module "email_audit_log_lambda" {
#   source        = "./modules/aws/lambda"
#   function_name = "${local.env}-email-audit-log"
#   description   = "lambda to persist email changes in timestream db as audit log"
#   role_arn      = aws_iam_role.content_moderation.arn

#   environment = {
#     REGION = var.region
#   }

#   tags = {
#     Environment = local.env
#   }
# }

# #-----------------------------------------------------------------------------------
# # setup the api gateway
# #-----------------------------------------------------------------------------------
# module "api" {
#   source                  = "./modules/aws/api"
#   description             = "Content Moderation API"
#   name                    = "${local.env}-content-moderation-api"
#   title                   = "${title(local.env)} Content Management API"
#   env                     = local.env
#   region                  = var.region
#   api_deployed_at         = var.api_deployed_at
#   create_email_lambda_arn = module.create_email_lambda.function_arn
# }

# #-----------------------------------------------------------------------------------
# # persist some k/v to consul store
# #-----------------------------------------------------------------------------------
# resource "consul_keys" "content_moderation" {
#   token = var.consul_token
#   # count      = length(var.consul_datacenter_list)
#   # datacenter = element(var.consul_datacenter_list, count.index)

#   key {
#     path   = "${local.env}-${var.domain}-kv/aurora/db_arn"
#     value  = module.aurora.arn
#     delete = true
#   }

#   key {
#     path   = "${local.env}-${var.domain}-kv/aurora/id"
#     value  = module.aurora.id
#     delete = true
#   }

#   key {
#     path   = "${local.env}-${var.domain}-kv/aurora/cluster_identifier"
#     value  = module.aurora.cluster_identifier
#     delete = true
#   }

#   key {
#     path   = "${local.env}-${var.domain}-kv/aurora/database_name"
#     value  = module.aurora.database_name
#     delete = true
#   }

#   key {
#     path   = "${local.env}-${var.domain}-kv/aurora/port"
#     value  = module.aurora.port
#     delete = true
#   }

#   key {
#     path   = "${local.env}-${var.domain}-kv/aurora/endpoint"
#     value  = module.aurora.endpoint
#     delete = true
#   }

#   key {
#     path   = "${local.env}-${var.domain}-kv/aurora/read_endpoint"
#     value  = module.aurora.read_endpoint
#     delete = true
#   }

#   key {
#     path   = "${local.env}-${var.domain}-kv/timestream/database_name"
#     value  = module.timestream.database_name
#     delete = true
#   }

#   key {
#     path   = "${local.env}-${var.domain}-kv/timestream/database_arn"
#     value  = module.timestream.database_arn
#     delete = true
#   }

#   key {
#     path   = "${local.env}-${var.domain}-kv/timestream/table_name"
#     value  = module.timestream.table_name
#     delete = true
#   }

#   key {
#     path   = "${local.env}-${var.domain}-kv/timestream/table_arn"
#     value  = module.timestream.table_arn
#     delete = true
#   }
# }
