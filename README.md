## Content Moderation Infrastructure

### Prerequisites
1. install and configure `aws cli` if you don't have one already
   - https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html
2. for convenience, isntall `tfenv` for managing different versions of terraform
   - https://github.com/tfutils/tfenv
   - `tfenv install 0.15.4`

### Testing with local state
1. install `consul` from source/docker
   1. https://www.consul.io/docs/install
   2. `consul agent -dev -node machine`
   3. dashboard: http://127.0.0.1:8500
2. install `vault` from source/docker
   1. https://learn.hashicorp.com/tutorials/vault/getting-started-install
   2. `export VAULT_ADDR='http://127.0.0.1:8200'`
   3. `export VAULT_TOKEN="{TOKEN_HERE}"`
   4. check vault status `vault status`
3. Initiate terraform:
   ```tf
   terraform init \
      -backend-config="address={consul_address}" \
      -backend-config="path={path}" \
      -backend-config="scheme=https"
   ```
   When testing locally, specifying path is enough.
4. Export auth related vars:
   ```sh
   export AWS_ACCESS_KEY_ID="anaccesskey"
   export AWS_SECRET_ACCESS_KEY="asecretkey"
   ```
   You can also add `profile = var.profile` to `aws` provider
5. Create new workspace, plan, apply, test and destroy the stack:
   1. `terraform workspace new dev`
   2. `terraform plan`
   3. `terraform apply`
   4. `terraform workspace select dev`
   5. `terraform destroy --auto-approve`

<!-- aws lambda update-function-code --function-name=ingest_email --zip-file=fileb://main.zip --profile=lms --region=eu-central-1

terraform init \
    -backend-config="address=" \
    -backend-config="path=content_moderation/terraform_state" \
    -backend-config="scheme=https"

terraform init -backend-config="path=content_moderation/terraform_state" \ -->
